
$(document).ready(function(){

    $('#submit').click(function(){

         //recuperation des données
        let nom = $("#nom").val().trim(); //en javascript document.getElementById('nom')
        let prenom= $("#prenom").val().trim(); //le .trim() élimine les espaces droite et gauche
        let email= $("#email").val().trim();
        let message= $("#msg").val().trim();

        // let chaine = $("form").serialize(); //recup toutes les valeurs du form
        let chaine = $("form").serializeArray();

        console.log ('chaine: ',chaine);


                $('#error1').empty();
                $('#error2').empty();
                $('#error3').empty();
                $('#error4').empty();

            if( nom.length === 0 || prenom == "" || email.length === 0||message.length === 0){

                if ( nom.length === 0){  
                    $("#error1").html('Merci de remplir ce champ'); 
                    // $('#nom').after('<span class="alerte">Merci de remplir ce champ</span>');
                };

                if( prenom == ""){
                // alert('Les champs nom et prénom sont requis');
                    $("#error2").html('Merci de remplir ce champ'); 
                    // $('#prenom').after('<span class="alerte">Merci de remplir ce champ</span>');
                };
                if ( email.length === 0){   
                    // $('#email').after('<span class="alerte">Merci de remplir ce champ</span>');
                    $("#error3").html('Merci de remplir ce champ'); 
                };
                if ( message.length === 0){   
                    // $('#msg').after('<span class="alerte">Merci de remplir ce champ</span>');
                    $("#error4").html('Merci de remplir ce champ'); 
                };
                return false;  //stoppe le script car renvoie un click non validé
            };

           


        $.ajax({

            type: "POST",
            url: "soumission.php",
            data: {
                Nom: nom,
                Prénom: prenom,
                Email: email,
                Message: message,
            },
            cache: false, //empeche le navigateur d'utiliser le cache
            
            success: function(reponse){
                alert(reponse);
            },

            error: function(xhr, status,error){
                console.error('xhr: ', xhr); //affiche dans la console sous format erreur et pas un simple log

            },
        });

               
    });

    

});


